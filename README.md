# Customer API

Customer API merupakan API yang dibuat dengan menggunakan PHP framework Laravel dan dapat melakukan CRUD customer dengan end point yang telah disediakan, seperti:

- List Of Customer(**GET** /api/customer).
- Detail Of Customer(**GET** /api/customer/{id}).
- Add New Customer(**POST** /api/customer).
- Add New Address(**POST** /api/address).
- Update Customer(**PATCH** /api/customer/{id}).
- Update Address(**PATCH** /api/address/{id}).
- Delete Customer(**DELETE** /api/customer/{id}).
- Delete Address(**DELETE** /api/address/{id}).

## Running Project

Berikut ini merupakan langkah langkah untuk menjalankan project:

- Run git clone <my-cool-project>
- Run composer install
- Run cp .env.example .env
- Run php artisan key:generate
- Run php artisan migrate
- Run php artisan serve
- Try one of the endpoints above

## Unit Test

Terdapat dua unit test yaitu CustomerTest dan AddressTest, berikut ini merupakan test pada CustomerTest:

- Test create new customer (php artisan test --filter test_create_new_customer)
- Test update data customer (php artisan test --filter test_update_data_customer)
- Test failed to create data customer (php artisan test --filter test_failed_to_create_data_customer)
- Test failed to update data customer (php artisan test --filter test_failed_to_update_data_customer)
- Test get all customer (php artisan test --filter test_get_all_customer)
- Test get one customer (php artisan test --filter test_get_one_customer)
- Test delete data customer (php artisan test --filter test_delete_data_customer)

Berikut test untuk AddressTest:

- Test create new address (php artisan test --filter test_create_new_address)
- Test update data address (php artisan test --filter test_update_data_address)
- Test failed to update data customer (php artisan test --filter test_failed_to_update_data_address)
- Test failed to create data customer (php artisan test --filter test_failed_to_create_data_address)
- Test delete data address (php artisan test --filter test_delete_data_address)

**MADE BY NADIR RUAYUANDA BAKRI**
